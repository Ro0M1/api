const { Console } = require("console");
const express = require("express");
const app = express();

const deuxTech = {
  tristan: "Je kiff Symphony2",
  romain: "J'ai plutôt un profil réseau",
  Valentin: "La catho me manque",
};

app.get("/ping", (req, res) => {
  res.send("Pong");
});

app.get("/eleves/:prenom", (req, res) => {
  const prenom = req.params.prenom;
  const eleves = deuxTech[prenom];

  if (eleves) {
    res.json(eleves);
  } else {
    res.status(404);
    res.json({ message: "L'élève n'existe pas" });
  }
});

app.listen(1234, () => console.log("Listen..."));
